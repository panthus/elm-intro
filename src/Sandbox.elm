module Main exposing (main)

import Browser
import Html exposing (Html, button, div, form, input, text)
import Html.Attributes exposing (type_)
import Html.Events exposing (onInput, onSubmit)
import Validation exposing (Validate, isValid, singleton, toMaybe, unwrap, unwrapError)
import ValidationRules exposing (maxLength, maxValue, minLength, minValue, parseInt, required)


type alias Model =
    { name : Validate String String
    , age : Validate String (Maybe Int)
    , message : String
    }


initialModel : Model
initialModel =
    { name = singleton "", age = singleton Nothing, message = "" }


validateName : Validate String String -> Validate String String
validateName =
    required (not << String.isEmpty)
        >> minLength 2 String.length
        >> maxLength 128 String.length


validateAge : Validate String (Maybe Int) -> Validate String (Maybe Int)
validateAge =
    minValue 0 (Maybe.withDefault 0)
        >> maxValue 150 (Maybe.withDefault 0)


validateForm : Model -> Model
validateForm ({ name, age } as model) =
    { model
        | name = validateName name
        , age = validateAge age
    }


type Msg
    = SetName String
    | SetAge String
    | Submit


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetName name ->
            { model | name = singleton name |> validateName }

        SetAge age ->
            { model | age = singleton age |> parseInt |> validateAge }

        Submit ->
            validateForm model


view : Model -> Html Msg
view model =
    form [ onSubmit Submit ]
        [ div []
            [ input [ type_ "text", onInput SetName ] []
            , div [] [ unwrapError (text "") text model.name ]
            ]
        , div []
            [ input [ type_ "text", onInput SetAge ] []
            , div [] [ unwrapError (text "") text model.age ]
            ]
        , button [ type_ "submit" ] [ text "Submit" ]
        , text model.message
        ]


main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , view = view
        , update = update
        }
