module ValidationRules exposing (maxLength, maxValue, minLength, minValue, parseInt, required)

import Validation exposing (Validate, validateWith)


required : (a -> Bool) -> Validate String a -> Validate String a
required f v =
    validateWith f identity "The field is required." v


parseInt : Validate String String -> Validate String (Maybe Int)
parseInt =
    let
        -- Empty string should become 0 because making a field required is not the responsibility of this function.
        toInt value =
            case value of
                "" ->
                    Just 0

                v ->
                    String.toInt v
    in
    validateWith ((/=) Nothing) toInt "The field must be a number."


minValue : Int -> (a -> Int) -> Validate String a -> Validate String a
minValue min f v =
    validateWith (\a -> f a >= min)
        identity
        ("The field has a minimum value of "
            ++ String.fromInt min
            ++ "."
        )
        v


maxValue : Int -> (a -> Int) -> Validate String a -> Validate String a
maxValue max f v =
    validateWith (\a -> f a <= max)
        identity
        ("The field has a maximum value of "
            ++ String.fromInt max
            ++ "."
        )
        v


minLength : Int -> (a -> Int) -> Validate String a -> Validate String a
minLength min f v =
    validateWith (\a -> f a >= min)
        identity
        ("The field has a minimum length of "
            ++ String.fromInt min
            ++ " character(s)."
        )
        v


maxLength : Int -> (a -> Int) -> Validate String a -> Validate String a
maxLength max f v =
    validateWith (\a -> f a <= max)
        identity
        ("The field has a maximum length of "
            ++ String.fromInt max
            ++ " character(s)."
        )
        v
