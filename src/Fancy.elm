module Main exposing (main)

import Browser
import Html exposing (Html, button, div, form, input, label, text)
import Html.Attributes exposing (class, for, id, type_)
import Html.Events exposing (onInput, onSubmit)
import Http exposing (Error, jsonBody, post, send)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import Json.Encode.Extra as Encode
import Maybe exposing (map2)
import Validation exposing (Validate, isInvalid, isValid, singleton, toMaybe, unwrap, unwrapError)
import ValidationRules exposing (maxLength, maxValue, minLength, minValue, parseInt, required)


type alias Model =
    { name : Validate String String
    , age : Validate String (Maybe Int)
    , message : String
    }


encodeModel : Model -> Maybe Value
encodeModel model =
    map2
        (\name age ->
            Encode.object
                [ ( "name", Encode.string name )
                , ( "age", Encode.maybe Encode.int age )
                ]
        )
        (toMaybe model.name)
        (toMaybe model.age)


decodeModel : Decoder Model
decodeModel =
    Decode.map3 Model
        (Decode.field "name" <| Decode.map singleton Decode.string)
        (Decode.field "age" <| Decode.map singleton <| Decode.maybe Decode.int)
        (Decode.succeed "")


initialModel : flags -> ( Model, Cmd Msg )
initialModel _ =
    ( { name = singleton "", age = singleton Nothing, message = "" }, Cmd.none )


validateName : Validate String String -> Validate String String
validateName =
    required (not << String.isEmpty)
        >> minLength 2 String.length
        >> maxLength 128 String.length


validateAge : Validate String (Maybe Int) -> Validate String (Maybe Int)
validateAge =
    minValue 0 (Maybe.withDefault 0)
        >> maxValue 150 (Maybe.withDefault 0)


validateForm : Model -> Model
validateForm ({ name, age } as model) =
    { model
        | name = validateName name
        , age = validateAge age
    }


isFormValid : Model -> Bool
isFormValid { name, age } =
    isValid name && isValid age


type Msg
    = SetName String
    | SetAge String
    | Submit
    | ReceiveSubmit (Result Error Model)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetName name ->
            ( { model | name = singleton name |> validateName }, Cmd.none )

        SetAge age ->
            ( { model | age = singleton age |> parseInt |> validateAge }, Cmd.none )

        Submit ->
            let
                validatedForm =
                    validateForm model
            in
            case ( isFormValid validatedForm, encodeModel validatedForm ) of
                ( True, Just body ) ->
                    ( { validatedForm | message = "Loading..." }
                    , post "https://reqres.in/api/users" (jsonBody body) decodeModel |> send ReceiveSubmit
                    )

                _ ->
                    ( validatedForm, Cmd.none )

        ReceiveSubmit (Ok m) ->
            ( { model | message = "Successfully created user '" ++ unwrap identity m.name ++ "'." }, Cmd.none )

        ReceiveSubmit (Err _) ->
            ( { model | message = "Something went wrong." }, Cmd.none )


isInvalidClass v =
    if isInvalid v then
        "is-invalid"

    else
        ""


view : Model -> Html Msg
view model =
    form [ onSubmit Submit, class "card card-body" ]
        [ div [ class "form-group" ]
            [ label [ for "name" ] [ text "Name" ]
            , input [ id "name", type_ "text", class <| "form-control " ++ isInvalidClass model.name, onInput SetName ]
                []
            , div [ class "invalid-feedback" ] [ unwrapError (text "") text model.name ]
            ]
        , div [ class "form-group" ]
            [ label [ for "age" ] [ text "Age (optional)" ]
            , input [ id "age", type_ "text", class <| "form-control " ++ isInvalidClass model.age, onInput SetAge ] []
            , div [ class "invalid-feedback" ] [ unwrapError (text "") text model.age ]
            ]
        , button [ type_ "submit", class "btn btn-primary" ] [ text "Submit" ]
        , text model.message
        ]


main : Program () Model Msg
main =
    Browser.element
        { init = initialModel
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }
