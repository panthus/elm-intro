module Validation exposing (Validate, isValid, isInvalid, singleton, toMaybe, unwrap, unwrapError, validateWith)


type Validate e a
    = Unknown a
    | Valid a
    | Invalid e a


singleton : a -> Validate e a
singleton a =
    Unknown a


unwrap : (a -> b) -> Validate e a -> b
unwrap f v =
    case v of
        Unknown a ->
            f a

        Valid a ->
            f a

        Invalid _ a ->
            f a


unwrapError : b -> (e -> b) -> Validate e a -> b
unwrapError b f v =
    case v of
        Unknown _ ->
            b

        Valid _ ->
            b

        Invalid e _ ->
            f e


isValid : Validate e a -> Bool
isValid v =
    case v of
        Valid _ ->
            True

        _ ->
            False


isInvalid : Validate e a -> Bool
isInvalid v =
    case v of
        Invalid _ _ ->
            True

        _ ->
            False


toMaybe : Validate e a -> Maybe a
toMaybe v =
    case v of
        Valid value ->
            Just value

        _ ->
            Nothing


validateWith : (b -> Bool) -> (a -> b) -> e -> Validate e a -> Validate e b
validateWith predicate f e v =
    let
        validate_ a =
            if predicate a then
                Valid a

            else
                Invalid e a
    in
    case v of
        Unknown a ->
            validate_ (f a)

        -- If combining validation rules a valid result can become invalid.
        Valid a ->
            validate_ (f a)

        -- If combining validation rules an invalid result should stay invalid.
        Invalid e_ a ->
            Invalid e_ (f a)
