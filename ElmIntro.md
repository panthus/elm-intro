# Elm, making front-ends simple
###### By Tolga Cerrah & Teun van Gisbergen

---

# Content

- The good bits
- Syntax & architecture
- A real world problem

---

# Development

- Good documentation and tooling
- Immutability & pure functions
- Effects
- Strong static types (no exceptions, no null/undefined)
- Time traveling debugger (import/export states)
- Very friendly compiler messages

---

# Elm compiler

![150%](./figures/compiler-error-message.PNG)

---

# Testing

- Pure functions mean easier testing
- Enough to just test the update function of the program as it is the only way to "change" the state

---

# Speed

![100%](./figures/fast-elm.png)

Graph taken from elm-lang.org

---

# Size

![100%](./figures/asset-sizes.png)

Graph taken from elm-lang.org, for RealWorld app see realworld.io

---

# Syntax
```elm
-- Record
type alias UserInfo =
    { firstname : String
    , lastname : String
    }

-- Sum type
type User
    = ProjectLeader UserInfo
    | Designer UserInfo
    
-- Tuple
type alias Department = (String, List User)
```

---

# Syntax

```elm
-- Function with a case expression and pattern matching
getName : User -> String
getName account =
    let
    	toName { firstname, lastname } =
        	firstname ++ " " ++ lastname
    in
    case account of
    	ProjectLeader info -> 
            toName info
        Designer info -> 
            toName info
```
Note: Return is implicit, every case results in the same type of expression.

---

# Syntax

```elm
-- Partial application
add : Int -> Int -> Int
add a b =
  a + b

add5 : Int -> Int
add5 =
  add 5

-- Function composition
add10 : Int -> Int
add10 =
  add5 >> add5

-- Pipes
add10toListSum : List Int -> Int
add10toListSum list =
  List.foldl (+) 0 list
    |> add10
```

---

# Elm architecture

<p align="center">
	<img src="./figures/diagram-tea.png"/>
</p>

---

# Elm architecture
```elm
import Browser as B
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)

main =
  B.sandbox { init = 0, update = update, view = view }

type Msg = Increment | Decrement

update msg model =
  case msg of
    Increment ->
      model + 1
    Decrement ->
      model - 1

view model =
  div []
    [ button [ onClick Decrement ] [ text "-" ]
    , div [] [ text (String.fromInt model) ]
    , button [ onClick Increment ] [ text "+" ]
    ]
```

---

# How about a real world problem?

![140%](./figures/form.png)

---

# Field validation

```elm
type Validate e a
    = Unknown a
    | Valid a
    | Invalid e a
```

---

# Some helpers

- `singleton` construct a not validated value
- `unwrap` get the value regardless of the validation status
- `unwrapError` to display the error if it exists
- `isValid` check if the field is valid
- `isInvalid` check if the field is invalid
- `toMaybe` `Just` for a valid value, otherwise `Nothing`
- `validateWith` create a composable validation rule

---

# Composable validation rules

- `required` indicate the field must be filled in
- `parseInt` parse a `String` value to an `Int`
- `minValue` the minimum value of an `Int`
- `maxValue` the maximum value of an `Int`
- `minLength` the minimum lenght of a `String`
- `maxLength` the maximum value of a `String`

---

# Lets compose

```elm
validateName : Validate String String -> Validate String String
validateName =
    required (not << String.isEmpty)
        >> minLength 2 String.length
        >> maxLength 128 String.length
```

---

# The form in Elm architecture

---

# Thats the form, how about submitting it?

The `Cmd msg` data type is used to represent effects.

```elm
main : Program () Model Msg
main =
    Browser.element
        { init = initialModel
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }

initialModel : flags -> ( Model, Cmd Msg )
initialModel _ =
    ( { name = singleton "", ... }, Cmd.none )
    
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
    	...
        Submit ->
            ( validateForm model, Cmd.none )
```

---

# End
